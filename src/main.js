import Vue from 'vue';
import axios from 'axios'
import Vuex from 'vuex';
import store from './store.js';
import App from './App.vue';

 
Vue.use(Vuex, axios)

Vue.config.productionTip = false;

let project;
const jambalaya = document.querySelector('#jambalaya');
if (jambalaya){
	project = jambalaya.dataset.project
}

new Vue({
	el: '#jambalaya',
	data: {
        project: project
    },
	store,
	render: h => h(App)
})

