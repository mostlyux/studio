import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import FM from 'gray-matter';

Vue.use(Vuex);

export default new Vuex.Store({
   state: {
     count: 755,
     token: '',
     authState: 'out',
     projectId: '',
     currentView: '',
     //Home
     recentList: [
      {path: 'pages/comp/comp2.md', type: 'Page · Update', checked: true},
      {path: 'pages/comp/menu/main.md', type: 'Component · New', checked: false},
      {path: 'pages/page1.md', type: 'Page · Update', checked: true}
    ],
    //Editor
    currentlyEditing: '',
    currentlyEditingChange: false,
    hasEditorData: false,
    editorDataPath: '',
    editorDataContent: '',
    //Save
    changesList: [
    ],
    //Files 
    currentlyDragging: {name: '', path: ''},
    needFileTree: true,
    fileTree: [],
    selectedFolder: '',
   },
   getters: {
    gitLabRepositoryURL: state => {
      return `https://gitlab.com/api/v4/projects/${state.projectId}/repository`
    },
    //Save
    changesListCount: state => {
      return state.changesList.length
    },
    changesListChecked: state => {
      return state.changesList.filter(change => change.checked === true)
    },
    changesListCheckedCount: (state, getters) => {
      return getters.changesListChecked.length
    },
    //Files
    fileTreeFormated: state => {
      //TODO This is mutating the data, stop that
      let files = [];
      state.fileTree.forEach( function (item) {
        if (item.type === 'tree') {
          item.type = 'folder'
          files.push(item);
        }else if (item.name.indexOf('.md') > 0 ) {
          item.type = 'file'
          files.push(item);
        }
      });
      return files
    },
    fileTreeCount: (state, getters) => {
      return getters.fileTreeFormated.length;
    },
   },
   mutations: {
    setCount (state, amount) {
       state.count = state.count + amount;
     },
    setToken (state, token) {
      localStorage.setItem('jamtoken', token);
      state.token = token;
    },
    setAuthState (state, status) {
      state.authState = status
    },
    setProjectId (state, id) {
      state.projectId = id;
    },
    setCurrentView (state, value) {
      if (value) {
        state.currentView = value;
      }else {
        state.currentView = '';
      }
    },
    setCurrentlyEditing (state, value) {
      if (value) {
        state.currentlyEditingChange = true;
        state.currentView = 'editor';
        state.currentlyEditing = value;
      } else {
        state.currentlyEditingChange = true;
        state.currentView = '';
        state.currentlyEditing = '';
      }
    },
    //Editor
    setHasEditorData (state, value) {
      state.hasEditorData = value;
    },
    setEditorDataPath(state, value) {
      state.editorDataPath = value;
    },
    setEditorDataContent (state, value) {
      state.editorDataContent = value;
      //Check to see we have what we were editing has changed and
      //If it has reorder the change list and move it to the front of the line
      //TODO Revaulate how we are doing this
      if (state.currentlyEditingChange) {
        const filtered = state.changesList.filter(function(node){
          return node.path !== state.currentlyEditing;
        });
        const change = {path: state.currentlyEditing, type: 'Page · Update', content: value, checked: true}
        filtered.unshift(change);
        state.changesList = filtered;
        state.currentlyEditingChange = false;
      }else {
        state.changesList[0].content = value;
      }
    },
    //Save
    toggleChangesListItem (state, index) {
      state.changesList[index].checked = !state.changesList[index].checked;
    },
    clearChangeList (state) {
      const unchecked = state.changesList.filter(change => change.checked === false);
      state.changesList = unchecked;
    },
    //Files
    setNeedFileTree (state, status) {
      state.needFileTree = status;
    },
    setFileTree (state, data) {
      state.fileTree = data;
    },
    setSelectedFolder (state, id) {
      state.selectedFolder = id;
    },
    setDraggedFile (state, payload) {
      console.log(payload.name, payload.path)
      state.currentlyDragging = {name:payload.name, path:payload.path};
    },
    //Demo
    //TODO Remove
    addDemoData (state, payload){
      const item = {path: payload.path, type: 'Page · Update', content: payload.content, checked: true}
      state.changesList.push(item);
    },
  },
  actions: {
    logIn (context , token) {
      context.commit('setAuthState', 'loading');
      gitLab(context, token);
    },
    logOut (context) {
      localStorage.removeItem('jamtoken');
      context.commit('setToken', '');
      context.commit('setAuthState', 'out');
    },
    //Editor
    //TODO See if we need this or we can combine 
    getEditorData (context) {
      //TODO handle site setting case
      //Check if we have the data in the changeList, if we do use it.  If not load it
      const loadedData = context.state.changesList.find(item => item.path === context.state.currentlyEditing);
      if (loadedData) {
        context.commit('setEditorDataPath', loadedData.path);
        context.commit('setEditorDataContent', loadedData.content);
        context.commit('setHasEditorData', true);
      }else {
        context.commit('setHasEditorData', false);
        gitLabGetFile(context, context.state.currentlyEditing);
      }
    },
    //TODO Largely a duplicate of getEditorData
    loadEditorData (context, path) {
      context.commit('setCurrentlyEditing', path);
      //TODO handle site setting case
      //Check if we have the data in the changeList, if we do use it.  If not load it
      const loadedData = context.state.changesList.find(item => item.path === path);
      if (loadedData) {
        context.commit('setEditorDataPath', loadedData.path);
        context.commit('setEditorDataContent', loadedData.content);
        context.commit('setHasEditorData', true);
      }else {
        context.commit('setHasEditorData', false);
        gitLabGetFile(context, path);
      }
    },
    // Files
    getFileTree (context) {
      gitLabTree(context, context.state.token);
    },
    //Save
    saveChanges (context) {
      gitLabCommit(context);
    },
    //Demo
    //TODO Remove
    loadDemoData(context, path) {
      gitLabDemoData(context, path);
    }
  }
});

//Gitlab

function gitLab (context, token) {
  axios.defaults.headers.common['PRIVATE-TOKEN'] = token;
  axios.get(`https://gitlab.com/api/v4/user`)
  .then(res => {
    context.commit('setToken', token);
    context.commit('setAuthState', 'logged in');
    return
  })
  .catch(err => {
    context.commit('setAuthState', 'error');
    return
  });
}

function gitLabTree (context, token) {
  axios.defaults.headers.common['PRIVATE-TOKEN'] = token;
  axios.get(`${context.getters.gitLabRepositoryURL}/tree?recursive=1`)
  .then(res => {
    const thedata = res;
    context.commit('setNeedFileTree', false);
    context.commit('setFileTree', thedata.data);
    return
  })
  .catch(err => {
    console.log(err);
    return
  });
}

function gitLabGetFile (context, path) {
  console.log('Load the path ', path)
  const uri = encodeURIComponent(path)
  const url = `${context.getters.gitLabRepositoryURL}/files/${uri}/raw?ref=master`;
  axios.defaults.headers.common['PRIVATE-TOKEN'] = context.state.token;
  axios.get(url)
  .then(res => {
    const thedata = res;
    context.commit('setEditorDataPath', path);
    context.commit('setEditorDataContent',thedata.data);
    context.commit('setHasEditorData', true);
    return
  })
  .catch(err => {
    console.log('Error loading editor data', err);
    return
  });
}

function gitLabCommit (context) {
  let commitData = {
    "branch": "master",
    "commit_message": "Jambalaya commit",
    "actions": []
  }
  context.getters.changesListChecked.forEach( function (item){
    commitData.actions.push(
      {
        "action": "update",
        "file_path": item.path,
        "content": item.content,
      },
    )
  })
  console.log('Lets commit this beats', commitData);
  const url = `${context.getters.gitLabRepositoryURL}/commits`;
  axios.defaults.headers.common['PRIVATE-TOKEN'] = context.state.token;
  axios.post(url, commitData)
  .then(function (response) {
    console.log(response);
    context.commit('clearChangeList');
  })
  .catch(function (error) {
    console.log(error);
  });
}

//Helper
function char_count(str, letter) {
 var letter_Count = 0;
 for (var position = 0; position < str.length; position++) 
 {
    if (str.charAt(position) == letter) 
      {
      letter_Count += 1;
      }
  }
  return letter_Count;
}

//Demo 
//TODO Remove this
function gitLabDemoData (context, path) {
  const uri = encodeURIComponent(path)
  const url = `${context.getters.gitLabRepositoryURL}/files/${uri}/raw?ref=master`;
  axios.defaults.headers.common['PRIVATE-TOKEN'] = context.state.token;
  axios.get(url)
  .then(res => {
    const thedata = res;
    context.commit('addDemoData', {path: path, content: thedata.data});
    return
  })
  .catch(err => {
    console.log('Error loading editor data', err);
    return
  });
}