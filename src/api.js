import store from './store.js';
import axios from 'axios';

export default {
    logIn(url, inputToken) {
        let token = '';
        if (inputToken) {
            token = inputToken;
        }else if(store.state.token.length > 0 ){
            token = store.state.token;
        }else {
            console.log('We should log you out');
        }
        axios.defaults.headers.common['PRIVATE-TOKEN'] = token;
        return axios.get(`https://gitlabbb.com/api/v4/` + url)
        .then(res => {
          console.log("The user", res);
          store.dispatch('logIn', token);
          return res.data
        })
    },
    fetch(url, inputToken) {
        let token = '';
        if (inputToken) {
            token = inputToken;
        }else if(store.state.token.length > 0 ){
            token = store.state.token;
        }else {
            console.log('We should log you out');
        }
        axios.defaults.headers.common['PRIVATE-TOKEN'] = token;
        return axios.get(`https://gitlabbb.com/api/v4/` + url)
        .then(res => {
          console.log("The user", res);
          store.dispatch('logIn', token);
          return res.data
        })
        .catch(err => {
            return err
        });
    }
}